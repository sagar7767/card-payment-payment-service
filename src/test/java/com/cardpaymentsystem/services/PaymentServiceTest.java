package com.cardpaymentsystem.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.cardpaymentsystem.models.Payment;
import com.cardpaymentsystem.repository.PaymentRepository;

//@ExtendWith(MockitoExtension.class)
class PaymentServiceTest {

	@Mock
	private PaymentRepository paymentRepository;

	@InjectMocks
	private PaymentService paymentService;

	@BeforeEach
	public void Setup() {
		MockitoAnnotations.initMocks(this);
	}

	SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	Date date = new Date();
	String transactionTimeandDate = dateformat.format(date);
	
	Payment transaction1 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5100, 100, 0,
			transactionTimeandDate);

	Payment transaction2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 100, 0,
			transactionTimeandDate);

	Payment transaction3 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5300, 100, 0,
			transactionTimeandDate);


	@Test
	void testSaveTransaction() {

		paymentService.saveTransaction(transaction1);

		verify(paymentRepository, times(1)).save(transaction1);
	}

	@Test
	void testGetTransactionByCardNumber() {
		
		List<Payment> list = new ArrayList<Payment>();

		list.add(transaction1);
		list.add(transaction2);
		list.add(transaction3);

		when(paymentRepository.findByCardNumber(transaction1.getCardNumber())).thenReturn(list);

		List<Payment> transactionList = paymentService.getTransactionByCardNumber(transaction1.getCardNumber());

		assertEquals(3, transactionList.size());
		verify(paymentRepository, times(1)).findByCardNumber(transaction1.getCardNumber());
	}

	@Test
	void testGetTransactionByCustomerId() {

		List<Payment> list = new ArrayList<Payment>();

		list.add(transaction1);
		list.add(transaction2);
		list.add(transaction3);

		when(paymentRepository.findByCustomerId(transaction1.getCustomerId())).thenReturn(list);

		List<Payment> transactionList = paymentService.getTransactionByCustomerId(transaction1.getCustomerId());

		assertEquals(3, transactionList.size());
		verify(paymentRepository, times(1)).findByCustomerId(transaction1.getCustomerId());

	}

	@Test
	void testExistsByCardNumber() {

		when(paymentRepository.existsByCardNumber(transaction1.getCardNumber())).thenReturn(true);
		Boolean actual = paymentService.existsByCardNumber(transaction1.getCardNumber());

		assertEquals(true, actual);
		verify(paymentRepository, times(1)).existsByCardNumber(transaction1.getCardNumber());

	}

	@Test
	void testGetBalance() {

		List<Payment> list = new ArrayList<Payment>();
		list.add(transaction1);
		list.add(transaction2);
		list.add(transaction3);

		when(paymentRepository.existsByCardNumber(transaction1.getCardNumber())).thenReturn(true);
		when(paymentRepository.findByCardNumber(transaction1.getCardNumber())).thenReturn(list);

		int actual = paymentService.getBalance(transaction1.getCardNumber());

		assertEquals(5100, actual);
		verify(paymentRepository, times(1)).findByCardNumber(transaction1.getCardNumber());

	}

	@Test
	void testGetBalance1() {

		List<Payment> list = new ArrayList<Payment>();
		list.add(transaction1);
		list.add(transaction2);
		list.add(transaction3);

		when(paymentRepository.existsByCardNumber(transaction1.getCardNumber())).thenReturn(false);

		int actual = paymentService.getBalance(transaction1.getCardNumber());
		assertEquals(0, actual);

	}

}
