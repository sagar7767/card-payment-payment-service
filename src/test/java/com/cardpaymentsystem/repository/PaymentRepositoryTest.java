package com.cardpaymentsystem.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.cardpaymentsystem.models.Payment;

@DataJpaTest
class PaymentRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	PaymentRepository repository;

	@Test
	void testFindByCardNumber() {
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = new Date();
		String transactionTimeandDate = dateformat.format(date);

		Payment pay1 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5100, 100, 0,
				transactionTimeandDate);
		entityManager.persist(pay1);

		Payment pay2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 100, 0,
				transactionTimeandDate);
		entityManager.persist(pay2);

		Payment pay3 = new Payment(1234123411112223L, "Visa", 1L, "21-12-2030", 0, "Success", 5300, 100, 0,
				transactionTimeandDate);
		entityManager.persist(pay3);

		Iterable payments = repository.findByCardNumber(pay1.getCardNumber());

		assertThat(payments).hasSize(2).contains(pay1, pay2);

	}

	@Test
	void testFindByCustomerId() {
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = new Date();
		String transactionTimeandDate = dateformat.format(date);

		Payment pay1 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5100, 100, 0,
				transactionTimeandDate);
		entityManager.persist(pay1);

		Payment pay2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 100, 0,
				transactionTimeandDate);
		entityManager.persist(pay2);

		Payment pay3 = new Payment(1234123411112223L, "Visa", 1L, "21-12-2030", 0, "Success", 5300, 100, 0,
				transactionTimeandDate);
		entityManager.persist(pay3);

		Iterable payments = repository.findByCustomerId(pay1.getCustomerId());

		assertThat(payments).hasSize(3).contains(pay1, pay2, pay3);

	}

	@Test
	void testExistsByCardNumber() {
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = new Date();
		String transactionTimeandDate = dateformat.format(date);

		Payment pay1 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5100, 100, 0,
				transactionTimeandDate);
		entityManager.persist(pay1);

		Boolean payments = repository.existsByCardNumber(pay1.getCardNumber());

		assertThat(payments).isEqualTo(true);

	}

}
