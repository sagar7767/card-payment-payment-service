package com.cardpaymentsystem.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.cardpaymentsystem.models.Payment;
import com.cardpaymentsystem.repository.PaymentRepository;
import com.cardpaymentsystem.services.PaymentService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(PaymentController.class)
class PaymentControllerTest {

	SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	Date date = new Date();
	String transactionTimeandDate = dateformat.format(date);

	@MockBean
	PaymentRepository paymentRepository;

	@MockBean
	PaymentService paymentService;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void testGetcard() throws Exception {
		Payment transaction1 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5100, 100, 0,
				transactionTimeandDate);
		transaction1.setPaymentId(1L);
		transaction1.setCardNumber(1234123411112222L);
		transaction1.setCardType("Visa");
		transaction1.setCustomerId(1L);
		transaction1.setExpirationDate("21-12-2030");
		transaction1.setPaymentAmount(0);
		transaction1.setStatus("Success");
		transaction1.setBalance(5100);
		transaction1.setCredit(100);
		transaction1.setDebit(0);
		transaction1.setTransactionTime(transactionTimeandDate);
//		Payment transaction2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 100, 0,
//				transactionTimeandDate);
		List<Payment> transactions = new ArrayList<>();
		transactions.add(transaction1);
		// transactions.add(transaction2);

		when(paymentService.getTransactionByCustomerId(transaction1.getCustomerId())).thenReturn((transactions));
		mockMvc.perform(get("/api/payment/gettransaction/{customerId}", transaction1.getCustomerId()))
				.andExpect(status().isOk())
				// .andReturn().getResponse().getContentAsString();
				.andExpect(jsonPath("$[0].paymentId").value(transaction1.getPaymentId()))
				.andExpect(jsonPath("$[0].cardNumber").value(transaction1.getCardNumber()))
				.andExpect(jsonPath("$[0].cardType").value(transaction1.getCardType()))
				.andExpect(jsonPath("$[0].customerId").value(transaction1.getCustomerId()))
				.andExpect(jsonPath("$[0].expirationDate").value(transaction1.getExpirationDate()))
				.andExpect(jsonPath("$[0].paymentAmount").value(transaction1.getPaymentAmount()))
				.andExpect(jsonPath("$[0].status").value(transaction1.getStatus()))
				.andExpect(jsonPath("$[0].balance").value(transaction1.getBalance()))
				.andExpect(jsonPath("$[0].credit").value(transaction1.getCredit()))
				.andExpect(jsonPath("$[0].debit").value(transaction1.getDebit()))
				.andExpect(jsonPath("$[0].transactionTime").value(transaction1.getTransactionTime())).andDo(print());
	}

	@Test
	void testGetbalance() throws Exception {
		Payment transaction1 = new Payment();
		transaction1.setPaymentId(1L);
		transaction1.setCardNumber(1234123411112222L);
		transaction1.setCardType("Visa");
		transaction1.setCustomerId(1L);
		transaction1.setExpirationDate("21-12-2030");
		transaction1.setPaymentAmount(0);
		transaction1.setStatus("Success");
		transaction1.setBalance(5100);
		transaction1.setCredit(100);
		transaction1.setDebit(0);
		transaction1.setTransactionTime(transactionTimeandDate);

		Payment transaction2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 100, 0,
				transactionTimeandDate);

		List<Payment> transactions = new ArrayList<>();
		transactions.add(transaction1);
		transactions.add(transaction2);

		Mockito.when(paymentService.getBalance(transaction1.getCardNumber())).thenReturn((transaction1.getBalance()));
		Mockito.when(paymentRepository.existsByCardNumber(transaction1.getCardNumber())).thenReturn(true);
		Mockito.when(paymentRepository.findByCardNumber(transaction1.getCardNumber())).thenReturn(transactions);

		mockMvc.perform(get("/api/payment/getbalance/{cardNumber}", transaction1.getCardNumber()))
				.andExpect(status().isOk())
				// .andExpect(jsonPath("$.balance").value(transaction1.getBalance()))
				.andDo(print());

	}

	@Test
	void testCredit() throws Exception {
		Payment transaction2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 100, 0,
				transactionTimeandDate);

		Mockito.when(paymentService.saveTransaction(transaction2)).thenReturn(transaction2);

		mockMvc.perform(post("/api/payment/credit").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(transaction2))).andExpect(status().isOk()).andDo(print());

	}

	@Test
	void testCredit2() throws Exception {
		Payment transaction2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 0, 0,
				transactionTimeandDate);

		Mockito.when(paymentService.saveTransaction(transaction2)).thenReturn(transaction2);

		mockMvc.perform(post("/api/payment/credit").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(transaction2))).andExpect(status().isOk()).andDo(print());

	}

	@Test
	void testDebit() throws Exception {
		Payment transaction2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 0, 100,
				transactionTimeandDate);
		
		Mockito.when(paymentService.getBalance(transaction2.getCardNumber())).thenReturn(transaction2.getBalance());
		Mockito.when(paymentService.existsByCardNumber(transaction2.getCardNumber())).thenReturn(true);
		Mockito.when(paymentService.saveTransaction(transaction2)).thenReturn(transaction2);

		mockMvc.perform(post("/api/payment/debit").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(transaction2)))
				.andExpect(status().isOk())
				.andDo(print());

	}

	@Test
	void testDebit2() throws Exception {
		Payment transaction2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 0, 0,
				transactionTimeandDate);
		
		Mockito.when(paymentService.getBalance(transaction2.getCardNumber())).thenReturn(transaction2.getBalance());
		Mockito.when(paymentService.existsByCardNumber(transaction2.getCardNumber())).thenReturn(true);
		Mockito.when(paymentService.saveTransaction(transaction2)).thenReturn(transaction2);

		mockMvc.perform(post("/api/payment/debit").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(transaction2)))
				.andExpect(status().isOk())
				.andDo(print());

	}

	@Test
	void testDebit3() throws Exception {
		Payment transaction2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 0, 100,
				transactionTimeandDate);
		
		Mockito.when(paymentService.getBalance(transaction2.getCardNumber())).thenReturn(transaction2.getBalance());
		Mockito.when(paymentService.existsByCardNumber(transaction2.getCardNumber())).thenReturn(false);
		Mockito.when(paymentService.saveTransaction(transaction2)).thenReturn(transaction2);

		mockMvc.perform(post("/api/payment/debit").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(transaction2)))
				.andExpect(status().isBadRequest())
				.andDo(print());

	}

	@Test
	void testDebit4() throws Exception {
		Payment transaction2 = new Payment(1234123411112222L, "Visa", 1L, "21-12-2030", 0, "Success", 5200, 0, 6000,
				transactionTimeandDate);
		
		Mockito.when(paymentService.getBalance(transaction2.getCardNumber())).thenReturn(transaction2.getBalance());
		Mockito.when(paymentService.existsByCardNumber(transaction2.getCardNumber())).thenReturn(true);
		Mockito.when(paymentService.saveTransaction(transaction2)).thenReturn(transaction2);

		mockMvc.perform(post("/api/payment/debit").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(transaction2)))
				.andExpect(status().isBadRequest())
				.andDo(print());

	}

}
