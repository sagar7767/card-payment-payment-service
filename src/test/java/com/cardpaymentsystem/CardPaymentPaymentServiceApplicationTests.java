package com.cardpaymentsystem;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootTest
@EnableEurekaClient
class CardPaymentPaymentServiceApplicationTests {

	@Test
	void contextLoads() {
		assertTrue(true);
	}

}
