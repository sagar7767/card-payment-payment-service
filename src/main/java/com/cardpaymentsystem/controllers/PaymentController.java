package com.cardpaymentsystem.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cardpaymentsystem.models.Payment;
import com.cardpaymentsystem.payload.request.PaymentRequest;
import com.cardpaymentsystem.payload.response.MessageResponse;
import com.cardpaymentsystem.repository.PaymentRepository;
import com.cardpaymentsystem.services.PaymentService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/payment")
public class PaymentController {

	@Autowired
	private PaymentService paymentService;

	@GetMapping("/gettransaction/{customerId}")
	public List<Payment> getcard(@PathVariable long customerId) {

		List<Payment> transaction = paymentService.getTransactionByCustomerId(customerId);
		return transaction;
	}

	@GetMapping("/getbalance/{cardNumber}")
	public int getbalance(@PathVariable long cardNumber) {
		return paymentService.getBalance(cardNumber);
	}

	@PostMapping("/credit")
	public ResponseEntity<?> credit(@RequestBody PaymentRequest paymentRequest) {
		int balance = 0;
		if (paymentRequest.getCredit() == 0) {
			return ResponseEntity.ok(new MessageResponse("Please enter the amount!"));
		}
		balance = paymentService.getBalance(paymentRequest.getCardNumber());
		balance = balance + paymentRequest.getCredit();

		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = new Date();
		String transactionTimeandDate = dateformat.format(date);

		Payment transaction = new Payment(paymentRequest.getCardNumber(), paymentRequest.getCardType(),
				paymentRequest.getCustomerId(), paymentRequest.getExpirationDate(), paymentRequest.getPaymentAmount(),
				"Closed", balance, paymentRequest.getCredit(), paymentRequest.getDebit(), transactionTimeandDate);
		paymentService.saveTransaction(transaction);
		return ResponseEntity.ok(new MessageResponse("Funds added successfully!"));

	}

	@PostMapping("/debit")
	public ResponseEntity<?> debit(@RequestBody PaymentRequest paymentRequest) {
		int balance = 0;
		if (paymentRequest.getDebit() == 0) {
			return ResponseEntity.ok(new MessageResponse("Please enter the amount!"));
		}
		balance = paymentService.getBalance(paymentRequest.getCardNumber());
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = new Date();
		String transactionTimeandDate = dateformat.format(date);

		if (paymentService.existsByCardNumber(paymentRequest.getCardNumber()) == false) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Add funds!"));
		}

		if (balance < paymentRequest.getDebit()) {
			Payment transaction = new Payment(paymentRequest.getCardNumber(), paymentRequest.getCardType(),
					paymentRequest.getCustomerId(), paymentRequest.getExpirationDate(),
					paymentRequest.getPaymentAmount(), "Failure", balance, paymentRequest.getCredit(),
					paymentRequest.getDebit(), transactionTimeandDate);
			paymentService.saveTransaction(transaction);

			return ResponseEntity.badRequest()
					.body(new MessageResponse("Error: Insufficient balance! Available balance is : " + balance));
		}

		balance = balance - paymentRequest.getDebit();

		Payment transaction = new Payment(paymentRequest.getCardNumber(), paymentRequest.getCardType(),
				paymentRequest.getCustomerId(), paymentRequest.getExpirationDate(), paymentRequest.getPaymentAmount(),
				"Closed", balance, paymentRequest.getCredit(), paymentRequest.getDebit(), transactionTimeandDate);
		paymentService.saveTransaction(transaction);
		return ResponseEntity.ok(new MessageResponse("Funds debited successfully!"));

	}

}
