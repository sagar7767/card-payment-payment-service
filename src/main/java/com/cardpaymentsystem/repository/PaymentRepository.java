package com.cardpaymentsystem.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cardpaymentsystem.models.Payment;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

	// Optional<Payment> findByCardNumber(long cardNumber);

	List<Payment> findByCardNumber(long cardNumber);

	List<Payment> findByCustomerId(long customerId);

	Boolean existsByCardNumber(long cardNumber);

	//Boolean existsByEmail(String email);

}
