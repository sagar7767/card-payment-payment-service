package com.cardpaymentsystem.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Builder;

@Builder
@Entity
@Table(name = "paymentdetails", uniqueConstraints = { @UniqueConstraint(columnNames = "paymentId") })
public class Payment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long paymentId;
	private long cardNumber;
	private String cardType;
	private Long customerId;
	private String expirationDate;
	private int paymentAmount;
	private String status;
	private int balance;
	private int credit;
	private int debit;
	private String transactionTime;

	public Payment() {
	}

	public Payment(long cardNumber, String cardType, Long customerId, String expirationDate,
			int paymentAmount, String status, int balance, int credit, int debit, String transactionTime) {
		super();
		//this.paymentId = paymentId;
		this.cardNumber = cardNumber;
		this.cardType = cardType;
		this.customerId = customerId;
		this.expirationDate = expirationDate;
		this.paymentAmount = paymentAmount;
		this.status = status;
		this.balance = balance;
		this.credit = credit;
		this.debit = debit;
		this.transactionTime = transactionTime;
	}
	
	

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public long getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public int getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(int paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public int getDebit() {
		return debit;
	}

	public void setDebit(int debit) {
		this.debit = debit;
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}

	@Override
	public String toString() {
		return "Payment [paymentId=" + paymentId + ", cardNumber=" + cardNumber + ", cardType=" + cardType
				+ ", customerId=" + customerId + ", expirationDate=" + expirationDate + ", paymentAmount="
				+ paymentAmount + ", status=" + status + ", balance=" + balance + ", credit=" + credit + ", debit="
				+ debit + ", transactionTime=" + transactionTime + "]";
	}

	public Payment(long paymentId, long cardNumber, String cardType, Long customerId, String expirationDate,
			int paymentAmount, String status, int balance, int credit, int debit, String transactionTime) {
		super();
		this.paymentId = paymentId;
		this.cardNumber = cardNumber;
		this.cardType = cardType;
		this.customerId = customerId;
		this.expirationDate = expirationDate;
		this.paymentAmount = paymentAmount;
		this.status = status;
		this.balance = balance;
		this.credit = credit;
		this.debit = debit;
		this.transactionTime = transactionTime;
	}

	
}
