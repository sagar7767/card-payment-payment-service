package com.cardpaymentsystem.services;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardpaymentsystem.models.Payment;
import com.cardpaymentsystem.repository.PaymentRepository;

@Service
public class PaymentService {

	@Autowired
	private PaymentRepository paymentRepository;

	public Payment saveTransaction(Payment payment) {
		return paymentRepository.save(payment);
	}

	public List<Payment> getTransactionByCardNumber(long cardNumber) {
		return paymentRepository.findByCardNumber(cardNumber);
	}

	public List<Payment> getTransactionByCustomerId(long customerId) {
		return paymentRepository.findByCustomerId(customerId);
	}

	public Boolean existsByCardNumber(long cardNumber) {
		return paymentRepository.existsByCardNumber(cardNumber);
	}

	public int getBalance(long cardNumber) {
		int balance = 0;
		if (paymentRepository.existsByCardNumber(cardNumber) == true) {
			List<Payment> transactions = paymentRepository.findByCardNumber(cardNumber);

			List<Payment> descSortedTransaction = transactions.stream()
					.sorted(Comparator.comparing(Payment::getTransactionTime).reversed()).collect(Collectors.toList());
			Payment latestTransaction = descSortedTransaction.get(0);
			balance = latestTransaction.getBalance();
			return balance;

		} else {
			return balance;
		}

	}

}
