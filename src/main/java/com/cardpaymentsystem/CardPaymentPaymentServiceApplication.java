package com.cardpaymentsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardPaymentPaymentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardPaymentPaymentServiceApplication.class, args);
	}

}
